<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $result_message[] ='Спасибо, результаты сохранены.';
    }
    

    $errors = array();

    $errors['field-name'] = !empty($_COOKIE['field-name_error']);
    if($errors['field-name']){
        setcookie('field-name_error', '', 100000);
        $result_message[] = '<div> Заполните имя.</div>';
    }

    $errors['field-email'] = !empty($_COOKIE['field-email_error']);
    if($errors['field-email']){
        setcookie('field-email_error', '', 100000);
        $result_message[] = '<div> Заполните почту.</div>';
    }

    $errors['field-date'] = !empty($_COOKIE['field-date_error']);
    if($errors['field-date']){
        setcookie('field-date_error', '', 100000);
        $result_message[] = '<div> Заполните дату рождения.</div>';
    }

    $errors['radio-sex'] = !empty($_COOKIE['radio-sex_error']);
    if($errors['radio-sex']){
        setcookie('radio-sex_error', '', 100000);
        $result_message[] = '<div> Выберите ваш пол.</div>';
    }

    $errors['radio-limb'] = !empty($_COOKIE['radio-limb_error']);
    if($errors['radio-limb']){
        setcookie('radio-limb_error', '', 100000);
        $result_message[] = '<div> Выберите количество конечностей.</div>';
    }

    $errors['ch'] = !empty($_COOKIE['ch_error']);
    if($errors['ch']){
        setcookie('ch_error', '', 100000);
        $result_message[] = '<div> Вы должны быть согласны с условиями.</div>';
    }

//Создаем массив значений полей и перепишем значения из формы
    $values = array();
    $values['field-name'] = empty($_COOKIE['value_of_field-name']) ? '' : $_COOKIE['value_of_field-name'];
    $values['field-email'] = empty($_COOKIE['value_of_field-email']) ? '' : $_COOKIE['value_of_field-email'];
    $values['field-date'] = empty($_COOKIE['value_of_field-date']) ? '' : $_COOKIE['value_of_field-date'];
    $values['radio-sex'] = empty($_COOKIE['value_of_radio-sex']) ? '' : $_COOKIE['value_of_radio-sex'];
    $values['radio-limb'] = empty($_COOKIE['value_of_radio-limb']) ? '' : $_COOKIE['value_of_radio-limb'];
    $values['ch'] = empty($_COOKIE['value_of_ch']) ? '' : $_COOKIE['value_of_ch'];
    $values['superpower'] = array();
    $values['superpower'][0] = empty($_COOKIE['superpower_0']) ? '' : $_COOKIE['superpower_0'];
    $values['superpower'][1] = empty($_COOKIE['superpower_1']) ? '' : $_COOKIE['superpower_1'];
    $values['superpower'][2] = empty($_COOKIE['superpower_2']) ? '' : $_COOKIE['superpower_2'];
    include('form.php');
}

// Проверка на корректность заполнения
else {
    $errors = false;

    if(empty($_POST['field-name'])){
        setcookie('field-name_error', '1', time() + 24*60*60);
        $errors = true;
    }
    else{
        setcookie('value_of_field-name', $_POST['field-name'], time() + 30*24*60*60);
    }
    
    if(!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['field-email'])){
        setcookie('field-email_error', '1', time() + 24*60*60);
        $errors = true;
    }
    else{
        setcookie('value_of_field-email', $_POST['field-email'], time() + 30*24*60*60);
    }

    if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['field-date'])){
        setcookie('field-date_error', '1', time() + 24*60*60);
        $errors = true;
    }else{
        setcookie('value_of_field-date', $_POST['field-date'], time() + 30*24*60*60);
    }

    if (!preg_match('/^[0-1]$/', $_POST['radio-sex'])) {
        setcookie('radio-sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        setcookie('value_of_radio-sex', $_POST['radio-sex'], time() + 30 * 24 * 60 * 60);
      }

    if (!preg_match('/^[0-4]$/', $_POST['radio-limb'])) {
    setcookie('radio-limb_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
    }
    else {
        setcookie('value_of_radio-limb', $_POST['radio-limb'], time() + 30 * 24 * 60 * 60);
    }

    if (!isset($_POST['ch'])) {
        setcookie('ch_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        setcookie('value_of_ch', $_POST['ch'], time() + 30 * 24 * 60 * 60);
      }

      foreach($_POST['superpower'] as $sup) {
        setcookie('superpower_value_' . $sup, 'true', time() + 30 * 24 * 60 * 60);
        }
    
        if ($errors) {
            header('Location: index.php');
            exit();
        }
    
        else{
            setcookie('field-name_error', '', 100000);
            setcookie('field-email_error', '', 100000);
            setcookie('radio-sex_error', '', 100000);
            setcookie('radio-limb_error', '', 100000);
            setcookie('field-date_error', '', 100000);
            setcookie('ch_error', '', 100000);
        }
    
        setcookie('save', '1');

    $conn = new PDO("mysql:host=localhost;dbname=u40980", 'u40980', '1404971', array(PDO::ATTR_PERSISTENT => true));

try{
    $user = $conn->prepare("INSERT INTO form SET name = ?, email = ?, gob = ?, sex = ?, limbs = ?, bio = ?, che = ?");
    $user -> execute([$_POST['field-name'], $_POST['field-email'], date('Y-m-d', strtotime($_POST['field-date'])), $_POST['radio-sex'], $_POST['radio-limb'], $_POST['BIO'], $_POST['ch']]);
    $id_user = $conn->lastInsertId();

    $user1 = $conn->prepare("INSERT INTO super SET id = ?, super_name = ?");
    $user1 -> execute([$id_user, $sup]);
    $result = true;
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}

setcookie('value_of_field-name', '', 100000);
setcookie('value_of_field-email', '', 100000);
setcookie('value_of_field-date', '', 100000);
setcookie('value_of_radio-sex', '', 100000);
setcookie('value_of_radio-limbs', '', 100000);
setcookie('value_of_ch', '', 100000);
header('Location: index.php');
}
